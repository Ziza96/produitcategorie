<?php

namespace AppBundle\Repository;

/**
 * ProduitRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProduitRepository extends \Doctrine\ORM\EntityRepository
{

  public function findAllJoinedToCategory()
  {
    return $this->createQueryBuilder('p')
        ->innerJoin('p.categorieId', 'c')
        ->addSelect('c')
        ->getQuery()->getResult();
  }

  public function findOneJoinedToCategory($produitId)
  {
    return $this->createQueryBuilder('p')
        ->innerJoin('p.categorieId', 'c')
        ->addSelect('c')
        ->where("p=:produitId")->setParameter("produitId", $produitId)
        ->getQuery()->getResult();
  }



  public function findOneByIdJoinedToCategory($categorieId)
  {
    return $this->createQueryBuilder('p')
        ->innerJoin('p.categorieId', 'c')
        // selects all the category data to avoid the query
        ->addSelect('c')
        ->where("c=:categorieId")->setParameter("categorieId", $categorieId)
        ->getQuery()->getResult();
  }
}
