<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Produit;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

      $em = $this->getDoctrine()->getManager();
      $produits = $em->getRepository('AppBundle:Produit')->findAllJoinedToCategory();

      $categories = $em->getRepository('AppBundle:Categorie')->findAll();

      return $this->render('default/index.html.twig', array(
          'produits' => $produits,'categories'=>$categories
      ));
    }

}
