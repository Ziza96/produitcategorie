-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3307
-- Généré le :  Dim 22 juil. 2018 à 13:34
-- Version du serveur :  10.2.8-MariaDB
-- Version de PHP :  5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `symfonyewill`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id`, `Nom`) VALUES
(1, 'Livre'),
(2, 'Film'),
(3, 'Série'),
(4, 'Musique'),
(5, 'Concert');

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Dt_ajout` date NOT NULL,
  `categorieId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_29A5EC27FE278E99` (`categorieId`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`id`, `Nom`, `Description`, `Image`, `Dt_ajout`, `categorieId`) VALUES
(2, 'OutLander Tome 1', '1945. Claire, jeune infirmière, retrouve son mari Frank Randall dans un village écossais pour une seconde lune de miel - la première a été interrompue par la guerre. Alors qu\'elle se promène dans la lande, elle découvre un ancien site mégalithique où les villageoises se réunissent en secret pour célébrer d\'étranges rites. Fascinée, elle s\'approche d\'un grand menhir fendu... et se volatilise. \r\nQuand elle reprend conscience, elle est entourée d\'hommes costumés qui se livrent bataille. Et curieusement, l\'un des combattants est le sosie de son mari... A sa grande stupeur, elle comprend bientôt qu\'elle est propulsée... en l\'an de grâce 1743 ! Période troublée s\'il en fut : l\'Écosse, occupée par les Anglais, est à feu et à sang...\r\nAinsi commence une épopée sauvage et baroque où se mêlent fantastique et histoire, action, amour et humour.', 'https://secure.sogides.com/public/produits/9782/764/809/gr_9782764809310.jpg', '2018-07-15', 1),
(3, 'Split', 'Kevin Wendell Crumb souffre d\'un trouble dissociatif de l\'identité. Il échange régulièrement avec sa psychiatre dévouée, le docteur Fletcher, qui a déjà pu distinguer vingt-trois personnalités différentes s\'exprimer à tour de rôle durant leurs conversations. Selon Fletcher, Kevin aurait subi des maltraitances et des humiliations au cours de son histoire et l’une de ses personnalités, plus sombre et plus menaçante que toutes les autres, nommée « la bête », demeure encore enfouie. Le docteur Fletcher met en exergue le fait que son patient s\'est forgé ces nombreuses personnalités différentes dans un besoin existentiel de se protéger des autres.', 'http://cinema.mu/wp-content/uploads/2017/04/split-poster-500x760.jpg', '2018-07-16', 2),
(4, 'Rogue One', 'Dans une période de conflit, un groupe d\'improbables héros s\'unit pour voler les plans de l\'Étoile de la Mort, l\'arme de destruction massive de l\'Empire. Ce tournant décisif dans la chronologie Star Wars réunit des personnes ordinaires qui choisissent de faire des choses extraordinaires, pour défendre une noble cause.', 'https://vignette.wikia.nocookie.net/fr.starwars/images/a/ad/Rogue_One_Affiche_finale.jpg/revision/latest?cb=20161012213710', '2014-12-14', 2),
(5, 'Metallica - Master of Puppets', 'Master of Puppets, sorti le 3 mars 1986, est le troisième album studio du groupe de Thrash metal Metallica.', 'https://images-na.ssl-images-amazon.com/images/I/81581tQGPzL._SY355_.jpg', '2018-07-17', 4),
(6, 'Metallica - Hardwired to Self Distruct', 'Hardwired…to Self-Destruct est le dixième album studio du groupe de thrash metal Metallica, sorti le 18 novembre 2016.', 'http://www.hornsup.fr/uploads/images/cover/large/metallicaharwiredtoselfdestruct-1.jpeg', '2017-07-17', 4),
(8, 'Pulp FIction', 'L\'odyssée sanglante et .burlesque de petits malfrats dans la jungle de Hollywood à travers trois histoires qui s\'entremêlent. Réalisé par Quentin Tarantino.', 'https://static.fnac-static.com/multimedia/images_produits/ZoomPE/9/0/7/3333973140709/tsp20130901121828/Pulp-fiction.jpg', '2018-07-17', 2),
(9, 'Sons of Anarchy', 'Sons of Anarchy est une série télévisée américaine composée de 92 épisodes d’une durée de 39 à 81 minutes. \r\nL\'histoire se déroule à Charming, ville fictive du comté de San Joaquin en Californie. Une lutte de territoires entre dealers et trafiquants d\'armes vient perturber les affaires d\'un club de bikers (Motorcycle Club, ou MC en anglais). Ce club, nommé Sons of Anarchy Motorcycle Club Redwood Original, couramment abrégé en SAMCRO, fait régner l\'ordre dans Charming. Clay Morrow, président de SAMCRO et patron du garage Teller-Morrow, ainsi que Jackson Teller, vice-président, mènent le club.\r\n\r\nLes Sons of Anarchy sont à la fois craints par la population mais également très respectés et admirés pour leur code d’honneur et leur capacité à maintenir l’ordre et rendre justice dans les situations délicates.', 'http://idata.over-blog.com/3/26/36/69/Sons-of-Anarchy/sons-of-anarchy-poster-image1.jpg', '2013-01-01', 3),
(10, 'Pulp Fiction', 'Pulp Fiction, ou Fiction pulpeuse au Québec, est un film de gangsters américain réalisé par Quentin Tarantino et sorti en 1994. Le scénario est co-écrit par Tarantino et Roger Avary. Utilisant la technique de narration non linéaire, il entremêle plusieurs histoires ayant pour protagonistes des membres de la pègre de Los Angeles et se distingue par ses dialogues stylisés, son mélange de violence et d\'humour et ses nombreuses références à la culture populaire.', 'https://de1imrko8s7v6.cloudfront.net/movies/posters/pulp-fiction-poster_1373054527.jpg', '2013-01-01', 2),
(11, 'Le Seigneur des anneaux - Le retour du roi', 'Le Seigneur des anneaux : Le Retour du roi est un film américano-néo-zélandais réalisé par Peter Jackson, sorti en 2003. Adapté du livre Le Retour du roi de J. R. R. Tolkien, il incorpore également des événements du livre précédent, Les Deux Tours. \r\nLe film a été récompensé par plus de 11 oscars.', 'http://fr.web.img3.acsta.net/medias/nmedia/18/35/14/33/18366630.jpg', '2018-07-17', 2),
(12, 'Tryo - Mamagoubida', 'Mamagubida est le premier album du groupe Tryo sur lequel presque toutes les chansons sont enregistrées en direct lors de leurs premières tournées en live en Bretagne et à la MJC de Fresnes.', 'http://www.krinein.com/img_oc/big/120.jpg', '2018-07-17', 4),
(13, 'Walking Dead - Tome 1', 'Walking Dead est une série de comic books américains en noir et blanc, scénarisée par Robert Kirkman et dessinée par Tony Moore puis Charlie Adlard, publiée par Image Comics depuis 2003.', 'https://images-na.ssl-images-amazon.com/images/I/51ahYCg8F0L._SX323_BO1,204,203,200_.jpg', '2018-07-17', 1),
(14, 'Seven', 'Peu avant sa retraite, l\'inspecteur William Somerset, un flic désabusé, est chargé de faire équipe avec un jeune idéaliste, David Mills. Ils enquêtent tout d\'abord sur le meurtre d\'un homme obèse que son assassin a obligé à manger jusqu\'à ce que mort s\'ensuive. L\'enquête vient à peine de commencer qu\'un deuxième crime, tout aussi macabre, est commis, puis un troisième. Petit à petit, les deux policiers font le lien entre tous ces assassinats.', 'http://fr.web.img2.acsta.net/medias/nmedia/18/35/91/33/19255605.jpg', '2018-07-17', 2);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `produit`
--
ALTER TABLE `produit`
  ADD CONSTRAINT `FK_29A5EC27FE278E99` FOREIGN KEY (`categorieId`) REFERENCES `categorie` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
